from threading import Semaphore

class DiningPhilosophers:
    def __init__(self):
        self.forks = [Semaphore(1) for _ in range(5)]

    def wantsToEat(self,
                   philosopher: int,
                   pickLeftFork: 'Callable[[], None]',
                   pickRightFork: 'Callable[[], None]',
                   eat: 'Callable[[], None]',
                   putLeftFork: 'Callable[[], None]',
                   putRightFork: 'Callable[[], None]') -> None:
        left_fork = philosopher
        right_fork = (philosopher + 1) % 5

        if philosopher % 2 == 1:
            left_fork, right_fork = right_fork, left_fork

        self.forks[left_fork].acquire()
        self.forks[right_fork].acquire()

        pickLeftFork()
        pickRightFork()
        eat()
        putLeftFork()
        putRightFork()

        self.forks[left_fork].release()
        self.forks[right_fork].release()
